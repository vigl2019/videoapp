package com.example.videoapp.model;

import com.google.gson.annotations.SerializedName;

public class VideoTrack {

    @SerializedName("id")
    private int id;

    @SerializedName("trackNumber")
    private int trackNumber;

    @SerializedName("title")
    private String title;

    @SerializedName("url")
    private String url;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTrackNumber() {
        return trackNumber;
    }

    public void setTrackNumber(int trackNumber) {
        this.trackNumber = trackNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}