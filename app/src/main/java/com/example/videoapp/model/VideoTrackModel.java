package com.example.videoapp.model;

import com.example.videoapp.contract.IVideoTrackContract;
import com.example.videoapp.contract.IVideoTracksContract;
import com.example.videoapp.repository.VideoTrackRepository;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;

public class VideoTrackModel implements IVideoTrackContract.Model, IVideoTracksContract.Model {

    private VideoTrackRepository repository;

    public VideoTrackModel(VideoTrackRepository repository) {
        this.repository = repository;
    }

    @Override
    public void setVideoTrackId(int id) {
        repository.setVideoTrackId(id);
    }

    @Override
    public int getVideoTrackId() {
        return repository.getVideoTrackId();
    }

    @Override
    public VideoTrack getVideoTrackById() throws IOException, XmlPullParserException {
        return repository.getVideoTrackById();
    }

    @Override
    public List<VideoTrack> getVideoTracks() throws IOException, XmlPullParserException{
        return repository.getVideoTracks();
    }

    @Override
    public List<VideoTrack> getVideoTracksByPage(int itemsCount, int pageIndex) throws
                                                        IOException, XmlPullParserException{
        return repository.getVideoTracksByPage(itemsCount, pageIndex);
    }
}