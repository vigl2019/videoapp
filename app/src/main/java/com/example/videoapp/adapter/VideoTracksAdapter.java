package com.example.videoapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.videoapp.R;
import com.example.videoapp.model.VideoTrack;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoTracksAdapter extends RecyclerView.Adapter<VideoTracksAdapter.VideoTracksViewHolder> {

    private List<VideoTrack> videoTracks;
    private OnTitleVideoTrackClickListener listener;

    public VideoTracksAdapter(List<VideoTrack> videoTracks) {
        this.videoTracks = videoTracks;
    }

    public interface OnTitleVideoTrackClickListener {
        void onClick(VideoTrack videoTrack);
    }

    public void setListener(OnTitleVideoTrackClickListener listener) {
        this.listener = listener;
    }

    @NonNull
    @Override
    public VideoTracksAdapter.VideoTracksViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        CardView cardView = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.video_items, parent, false);

        return new VideoTracksAdapter.VideoTracksViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(@NonNull VideoTracksAdapter.VideoTracksViewHolder holder, int position) {
        VideoTrack videoTrack = videoTracks.get(position);
        holder.trackTitle.setText(videoTrack.getTitle());
    }

    @Override
    public int getItemCount() {
          return videoTracks.size();
    }

    public void refresh() {
        notifyDataSetChanged();
    }

    class VideoTracksViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.vi_track_title)
        TextView trackTitle;

        CardView cardView;

        public VideoTracksViewHolder(@NonNull CardView itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);
            cardView = itemView;

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null) {
                        listener.onClick(videoTracks.get(getAdapterPosition()));
                    }
                }
            });
        }
    }
}

