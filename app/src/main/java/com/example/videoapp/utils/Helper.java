package com.example.videoapp.utils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.example.videoapp.R;

public final class Helper {

    Context context = App.getInstance().getContext();

    public static String convertIntToString(int value){
        return String.valueOf(value);
    }

    public static void createAlertDialog(String errorMassage, Context context) {

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(context);
        dialogBuilder.setTitle(R.string.error_title_text)
                .setMessage(errorMassage)
                .setCancelable(false)
                .setPositiveButton(context.getString(R.string.ok_button_text), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                    }
                });

        AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();
    }
}