package com.example.videoapp.utils;

import android.app.Application;
import android.content.Context;

public final class App extends Application {

    private static App instance;
    private Context context;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        context = App.this;
    }

    public static App getInstance() {
        return instance;
    }
    public Context getContext() {
        return context;
    }
}
