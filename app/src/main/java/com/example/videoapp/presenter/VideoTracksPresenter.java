package com.example.videoapp.presenter;

import android.content.Context;

import com.example.videoapp.R;
import com.example.videoapp.contract.IVideoTracksContract;
import com.example.videoapp.model.VideoTrack;
import com.example.videoapp.utils.App;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;

public class VideoTracksPresenter implements IVideoTracksContract.Presenter {

    private IVideoTracksContract.View view;
    private IVideoTracksContract.Model model;

    private Context context = App.getInstance().getContext();

    public VideoTracksPresenter(IVideoTracksContract.View view, IVideoTracksContract.Model model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void initPresenter() {
        view.initView();
    }

    @Override
    public void loadVideoTracks() {

        try {
            List<VideoTrack> videoTracks = model.getVideoTracks();

            if ((videoTracks == null) || (videoTracks.size() == 0)) {
                view.showError(App.getInstance().getContext().getString(R.string.error_video_tracks));
            } else {
                view.loadDataIntoList(videoTracks);
            }
        }
        catch (XmlPullParserException | IOException e){
            view.showError(context.getString(R.string.error_from_xml_parser));
        }
    }

    @Override
    public void loadVideoTracksByPage(int itemsCount, int pageIndex) {

        try {
            List<VideoTrack> videoTracks = model.getVideoTracksByPage(itemsCount, pageIndex);

            if ((videoTracks == null) || (videoTracks.size() == 0)) {
                view.showError(App.getInstance().getContext().getString(R.string.error_video_tracks));
            } else {
                view.loadDataIntoList(videoTracks);
            }
        }
        catch (XmlPullParserException | IOException e){
            view.showError(context.getString(R.string.error_from_xml_parser));
        }
    }

    @Override
    public void detachView() {
        view = null;
    }
}

