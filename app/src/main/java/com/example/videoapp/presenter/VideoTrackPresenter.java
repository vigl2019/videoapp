package com.example.videoapp.presenter;

import android.content.Context;

import com.example.videoapp.R;
import com.example.videoapp.contract.IVideoTrackContract;
import com.example.videoapp.model.VideoTrack;
import com.example.videoapp.utils.App;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class VideoTrackPresenter implements IVideoTrackContract.Presenter {

    private IVideoTrackContract.View view;
    private IVideoTrackContract.Model model;

    private Context context = App.getInstance().getContext();

    public VideoTrackPresenter(IVideoTrackContract.View view, IVideoTrackContract.Model model) {
        this.view = view;
        this.model = model;
    }

    @Override
    public void initPresenter(int id) {
        model.setVideoTrackId(id);
        view.initView();
    }

    @Override
    public VideoTrack getVideoTrackById() {

        VideoTrack videoTrack = null;

        try {
            videoTrack = model.getVideoTrackById();
        } catch (XmlPullParserException | IOException e) {
            view.showError(context.getString(R.string.error_from_xml_parser));
        }

        return videoTrack;
    }

    @Override
    public int getVideoTrackId() {

        int id = model.getVideoTrackId();

        if (id <= 0) {
            view.showError(App.getInstance().getContext().getString(R.string.error_video_track_id));
        }
        return id;
    }

    @Override
    public void loadVideoTrackById() {

        try {
            VideoTrack videoTrack = model.getVideoTrackById();

            if (videoTrack == null) {
                view.showError(App.getInstance().getContext().getString(R.string.error_video_track));
            } else {
                view.loadData(videoTrack);
            }
        } catch (XmlPullParserException | IOException e) {
            view.showError(context.getString(R.string.error_from_xml_parser));
        }
    }

    @Override
    public void detachView() {
        view = null;
    }
}
