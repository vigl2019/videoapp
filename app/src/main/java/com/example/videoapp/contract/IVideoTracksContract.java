package com.example.videoapp.contract;

import com.example.videoapp.model.VideoTrack;

import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;

public interface IVideoTracksContract {

    interface View {

        void initView();

        void loadDataIntoList(List<VideoTrack> videoTracks);

        void showError(String message);
    }

    interface Presenter {

        void initPresenter();

        void loadVideoTracks();

        void loadVideoTracksByPage(int itemsCount, int pageIndex);

        void detachView();
    }

    interface Model{

        List<VideoTrack> getVideoTracks() throws IOException, XmlPullParserException;

        List<VideoTrack> getVideoTracksByPage(int itemsCount, int pageIndex) throws
                                                    IOException, XmlPullParserException;
    }
}
