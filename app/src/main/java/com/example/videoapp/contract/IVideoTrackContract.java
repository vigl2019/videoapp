package com.example.videoapp.contract;

import com.example.videoapp.model.VideoTrack;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public interface IVideoTrackContract {

    interface View {

        void initView();

        void loadData(VideoTrack videoTrack);

        void showError(String message);
    }

    interface Presenter {

        void initPresenter(int id);

        int getVideoTrackId();

        VideoTrack getVideoTrackById();

        void loadVideoTrackById();

        void detachView();
    }

    interface Model {

        void setVideoTrackId(int id);

        int getVideoTrackId();

        VideoTrack getVideoTrackById() throws IOException, XmlPullParserException;
    }
}