package com.example.videoapp.repository;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;

import com.example.videoapp.R;
import com.example.videoapp.model.VideoTrack;
import com.example.videoapp.utils.App;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

public final class VideoTrackRepository implements IVideoTrackRepository {

    private Context context = App.getInstance().getContext();
    private SharedPreferences sharedPreferences;

    @Override
    public int getVideoTrackId() {
        sharedPreferences = context.getSharedPreferences(context.getString(R.string.id), Context.MODE_PRIVATE);
        return sharedPreferences.getInt(context.getString(R.string.id), -1);
    }

    @Override
    public void setVideoTrackId(int id) {
        sharedPreferences = context.getSharedPreferences(context.getString(R.string.id), Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(context.getString(R.string.id), id);
        editor.apply();
    }

    @Override
    public VideoTrack getVideoTrackById() throws
            IOException, XmlPullParserException{
        int id = sharedPreferences.getInt(context.getString(R.string.id), -1);
        VideoTrack videoTrack = new VideoTrack();

            List<VideoTrack> videoTracks = getVideoTracks();

            for (VideoTrack track : videoTracks) {
                if (track.getId() == id) {
                    videoTrack = track;
                    break;
                }
            }

        return videoTrack;
    }

    @Override
    public List<VideoTrack> getVideoTracks() throws
            IOException, XmlPullParserException{
        List<VideoTrack> videoTracks;

        videoTracks = parseVideoTracksXML();
        return videoTracks;
    }

    @Override
    public List<VideoTrack> getVideoTracksByPage(int itemsCount, int pageIndex) throws
            IOException, XmlPullParserException{

        List<VideoTrack> videoTracks = getVideoTracks();
        List<VideoTrack> newVideoTracks = new ArrayList<>(itemsCount);

        int videoTracksCount = videoTracks.size();
//      int countPages = videoTracksCount / itemsCount;
        int startIndex = itemsCount * pageIndex;

        for (int i = startIndex; i < (startIndex + itemsCount); i++) {

            if (i < videoTracks.size()) {
                VideoTrack videoTrack = videoTracks.get(i);
                newVideoTracks.add(videoTrack);
            }
        }

        return newVideoTracks;
    }

    private ArrayList<VideoTrack> parseVideoTracksXML()  throws
            IOException, XmlPullParserException {
        XmlPullParserFactory parserFactory;
        AssetManager assetManager = context.getAssets();
        ArrayList<VideoTrack> videoTracks;

            parserFactory = XmlPullParserFactory.newInstance();
            XmlPullParser parser = parserFactory.newPullParser();

            String xmlFile = context.getString(R.string.video_tracks_list);
            InputStream inputStream = assetManager.open(xmlFile);

            parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            parser.setInput(inputStream, null);

            videoTracks = processParsingVideoTracksXML(parser);

        return videoTracks;
    }

    private ArrayList<VideoTrack> processParsingVideoTracksXML(XmlPullParser parser) throws
            IOException, XmlPullParserException {
        ArrayList<VideoTrack> videoTracks = new ArrayList<>();
        int eventType = parser.getEventType();
        VideoTrack currentVideoTrack = null;

        while (eventType != XmlPullParser.END_DOCUMENT) {

            String tagName;

            switch (eventType) {
                case XmlPullParser.START_TAG:
                    tagName = parser.getName();

                    if (tagName.equals("video_track")) {
                        currentVideoTrack = new VideoTrack();
                        videoTracks.add(currentVideoTrack);
                    } else if (currentVideoTrack != null) {
                        if (tagName.equals("id")) {
                            currentVideoTrack.setId(Integer.parseInt(parser.nextText()));
                        } else if (tagName.equals("track_number")) {
                            currentVideoTrack.setTrackNumber(Integer.parseInt(parser.nextText()));
                        } else if (tagName.equals("title")) {
                            currentVideoTrack.setTitle(parser.nextText());
                        } else if (tagName.equals("url")) {
                            currentVideoTrack.setUrl(parser.nextText());
                        }
                    }
                    break;
            }

            eventType = parser.next();
        }
        return videoTracks;
    }
}