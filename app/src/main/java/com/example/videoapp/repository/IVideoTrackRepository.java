package com.example.videoapp.repository;

import com.example.videoapp.model.VideoTrack;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;

public interface IVideoTrackRepository {

    int getVideoTrackId() throws IOException, XmlPullParserException;

    void setVideoTrackId(int id);

    VideoTrack getVideoTrackById() throws IOException, XmlPullParserException;

    List<VideoTrack> getVideoTracks() throws IOException, XmlPullParserException;

    List<VideoTrack> getVideoTracksByPage(int countItems, int pageIndex) throws
                                              IOException, XmlPullParserException;
}