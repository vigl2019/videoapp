package com.example.videoapp.view;

import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.videoapp.R;
import com.example.videoapp.contract.IVideoTrackContract;
import com.example.videoapp.model.VideoTrack;
import com.example.videoapp.model.VideoTrackModel;
import com.example.videoapp.presenter.VideoTrackPresenter;
import com.example.videoapp.repository.VideoTrackRepository;
import com.example.videoapp.utils.Helper;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VideoTrackActivity extends AppCompatActivity implements IVideoTrackContract.View {

    @BindView(R.id.avt_video_track_title)
    TextView trackTitle;

    @BindView(R.id.avt_video_track)
    VideoView videoView;

    @BindString(R.string.track_title_bundle)
    String trackTitleBundle;

    @BindString(R.string.instance_state_position)
    String position;

    private int videoPosition;
    private IVideoTrackContract.Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_track);
        ButterKnife.bind(this);

        int id = getIntent().getIntExtra(getString(R.string.video_track_id), -1);

        presenter = new VideoTrackPresenter(this, new VideoTrackModel(new VideoTrackRepository()));
        presenter.initPresenter(id);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(trackTitleBundle, trackTitle.getText().toString());
        outState.putInt(position, videoView.getCurrentPosition());
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        trackTitle.setText(savedInstanceState.getString(trackTitleBundle));
        videoPosition = savedInstanceState.getInt(position);
        videoView.seekTo(videoPosition);
    }

    @Override
    public void initView() {
        initActionBar();
        presenter.loadVideoTrackById();
    }

    private void initActionBar() {
        ActionBar actionBar = getSupportActionBar();

        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);

            VideoTrack videoTrack = presenter.getVideoTrackById();

            actionBar.setTitle(getString(R.string.track_number) + " " +
                    Helper.convertIntToString(videoTrack.getTrackNumber()));
        }
    }

    @Override
    public void loadData(VideoTrack videoTrack) {

        videoPosition = 0;

        trackTitle.setText(videoTrack.getTitle());

        videoView.setVideoPath(videoTrack.getUrl());
        final MediaController mediaController = new MediaController(this);
        videoView.setMediaController(mediaController);
//      mediaController.setMediaPlayer(videoView);
        mediaController.setAnchorView(videoView);
        videoView.setZOrderOnTop(true);

        videoView.requestFocus();

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mediaPlayer.seekTo(videoPosition);

                if (videoPosition == 0) {
                    mediaPlayer.start();
                }

                mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
                    @Override
                    public void onVideoSizeChanged(MediaPlayer mediaPlayer, int i, int i1) {
                        mediaController.setAnchorView(videoView);
                    }
                });
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    @Override
    public void showError(String message) {
        Helper.createAlertDialog(message, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        videoView.pause();
    }

    @Override
    public void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }
}
