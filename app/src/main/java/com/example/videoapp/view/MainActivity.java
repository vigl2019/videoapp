package com.example.videoapp.view;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.videoapp.R;
import com.example.videoapp.adapter.VideoTracksAdapter;
import com.example.videoapp.contract.IVideoTracksContract;
import com.example.videoapp.model.VideoTrack;
import com.example.videoapp.model.VideoTrackModel;
import com.example.videoapp.presenter.VideoTracksPresenter;
import com.example.videoapp.repository.VideoTrackRepository;
import com.example.videoapp.utils.Helper;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MainActivity extends AppCompatActivity implements IVideoTracksContract.View {

    private IVideoTracksContract.Presenter presenter;
    private VideoTracksAdapter adapter;

    @BindView(R.id.am_video_tracks_list)
    RecyclerView recyclerView;

    @BindView(R.id.am_tracks_title)
    TextView tracksTitle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);
    }

    @Override
    public void initView() {

        tracksTitle.setText(R.string.tracks_title);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        presenter.loadVideoTracks();
    }

    @Override
    public void loadDataIntoList(List<VideoTrack> videoTracks) {
        adapter = new VideoTracksAdapter(videoTracks);

        adapter.setListener(new VideoTracksAdapter.OnTitleVideoTrackClickListener() {
            @Override
            public void onClick(VideoTrack videoTrack) {
                Intent intent = new Intent(MainActivity.this, VideoTrackActivity.class);
                intent.putExtra(getString(R.string.video_track_id), videoTrack.getId());
                startActivity(intent);
            }
        });

        recyclerView.setAdapter(adapter);
    }

    @Override
    public void showError(String message) {
        Helper.createAlertDialog(message, this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        presenter = new VideoTracksPresenter(this, new VideoTrackModel(new VideoTrackRepository()));
        presenter.initPresenter();
    }

    @Override
    public void onDestroy() {
        presenter.detachView();
        super.onDestroy();
    }
}